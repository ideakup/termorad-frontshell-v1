<?php

namespace App\Http\Controllers;

use Auth;
use Redirect;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use App\User;
use App\Menu;
use App\MenuVariable;
use App\SiteSettings;
use App\Tag;
use App\TagVariable;
use App\Category;
use App\CategoryVariable;
use App\Language;

use App\MenuHasContent;
use App\ContentHasTag;
use App\ContentHasCategory;

class HomeController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index($lang, $slug, $attr = null, $param = null)
    {
        //DB::enableQueryLog();
        //dd(DB::getQueryLog());
        $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

        $menu = MenuVariable::where('slug', $slug)->first()->menu;
        $headerslug = $menu;
        while ($headerslug->top_id != null) {
            $headerslug = $headerslug->topMenu;
        }
        $headerslug = $headerslug->variables->where('lang_code', $lang)->first()->slug;

        $menuall = Menu::where('top_id', null)->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $mainmenu = Menu::where(
            function ($query){
                $query->where('position', 'all')->orWhere('position', 'top');
            }
        )->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();

        $topmenus = Menu::where('position', '!=', 'aside')->where('top_id', null)->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $asidemenus = Menu::where('position', '!=', 'top')->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

        $sitesettings = SiteSettings::all();

        $startDate = Carbon::now()->copy()->startOfDay()->subMonths(3);

        $tags = Tag::where('deleted', 'no')->where('status', 'active')->get();
        $tagCont = array();
        foreach ($menu->content()->get() as $cont) {
            foreach ($cont->tags()->get() as $tagg) {
                $tContExist = false;
                if (count($tagCont) == 0) {
                    $tagCont[] = $tagg;
                }else{
                    foreach ($tagCont as $tCont) {
                        if ($tagg->id != $tCont->id) {
                            $tContExist = true;
                        }
                    }
                    if ($tContExist) {
                        $tagCont[] = $tagg;
                    }
                }
            }
        }

        $categories = Category::where('deleted', 'no')->where('status', 'active')->get();
        $categoryCont = array();
        foreach ($menu->content()->get() as $cont) {
            foreach ($cont->categories()->get() as $catt) {
                $cContExist = false;
                if (count($categoryCont) == 0) {
                    $categoryCont[] = $catt;
                }else{
                    foreach ($categoryCont as $cCont) {
                        if ($catt->id != $cCont->id) {
                            $cContExist = true;
                        }
                    }
                    if ($cContExist) {
                        $categoryCont[] = $catt;
                    }
                }
            }
        }

        return view('home', array('langs' => $langs, 'menu' => $menu, 'menuall' => $menuall, 'mainmenu' => $mainmenu, 'topmenus' => $topmenus, 'asidemenus' => $asidemenus, 'sitesettings' => $sitesettings, 'tags' => $tags, 'tagCont' => $tagCont, 'categories' => $categories, 'categoryCont' => $categoryCont, 'headerslug' => $headerslug, 'lang' => $lang, 'slug' => $slug, 'attr' => $attr, 'param' => $param));
    }

}
