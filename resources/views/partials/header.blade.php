@php
    $segments = '';
    for ($i=1; $i < count(Request::segments()); $i++) { 
        $segments = $segments.'/'.Request::segments()[$i];
    }
@endphp

@if($__headertheme == 'home')
    
    <style type="text/css">
        @media (max-width: 991px){
            .top-menu2{
                display: none !important;
            }
            #navbarSupportedContent { 
                background-color: #FFF;
            }
            .ust-menu{
                margin-top: 0;
                padding: 20px;
            }
            .dropdown-menu{
                border: none;
            }
            .no-mobile{
                display: none;
            }
            .mobile-full{
                -ms-flex: 0 0 100%;
                flex: 0 0 100%;
                max-width: 100%;
            }
        }
    </style>

    <div class="container-fluid red-menu-bg" style="background-color: #E74639;  height: 50px;">
        <div class="container">
            <div class="row">

                <div class="col-6 no-mobile">
                    <p style="color: white; font-family: 'Roboto'; font-size: 12px; line-height: 50px; margin: 0;">info@termorad.com.tr | +90 0 332 582 0708</p>
                </div>
                
                <div class="col-6 mobile-full">
                    <div class="langTool" style="padding: 15px; float: right;">
                        @foreach ($langs as $lng)
                            <a class="langLink" href="{{ url($lng->code.$segments) }}" style="color: #FFFFFF">
                                {{ $lng->name }}
                            </a>
                            @if (!$loop->last)
                                <span class="langLink" style="color: #FFFFFF">|</span>
                            @endif
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    </div>

    <style type="text/css">
        .top-menu2{
            color: #FFFFFF;
            font-family: 'Open Sans', sans-serif;
            font-weight: 400;
            font-size: 12px;
        }

        .top-menu2:hover{
            color: #3B3A39;
            text-decoration: none;
        }
    </style>
    
    <div class="top-menu2" style="position: absolute; width: 460px; height: 55px; top: 0; text-align: center; z-index: 9999; color: #FFFFFF; font-family: 'Open Sans', sans-serif; font-weight: 400; font-size: 12px; margin-left: calc((100% - 460px) / 2);">
        <a href="https://tahsilat.termorad.com.tr" target="_blank"><img src="{{ url('images/onlineOdemeIcon'.Request::segment(1).'.png') }}" width="125" height="55"></a>
        <a href="https://www.dreamreality.com.tr/3d-model/termorad-unicera-2019/fullscreen" target="_blank"><img src="{{ url('images/SanalTurIcon'.Request::segment(1).'.png') }}" width="125" height="55"></a>
        &nbsp; | &nbsp;

        @if (Request::segment(1) == 'en')
            <a href="{{ url('katalog/termorad_katalog_en.pdf') }}" class="top-menu2" target="_blank">E-CATALOG</a>
            &nbsp; | &nbsp;
            <a href="{{ url('en/iletisim') }}" class="top-menu2">CONTACT</a>
        @elseif (Request::segment(1) == 'de')
            <a href="{{ url('katalog/termorad_katalog_de.pdf') }}" class="top-menu2" target="_blank">KATALOG</a>
            &nbsp; | &nbsp;
            <a href="{{ url('de/iletisim') }}" class="top-menu2">KONTAKT</a>
        @else
            <a href="{{ url('katalog/termorad_katalog_tr.pdf') }}" class="top-menu2" target="_blank">E-KATALOG</a>
            &nbsp; | &nbsp;
            <a href="{{ url('tr/iletisim') }}" class="top-menu2">İLETİŞİM</a>
        @endif
        
    </div>

    
@endif

<!-- <header class="@if($__headertheme == 'home') slider-background @endif"> -->
<header>
    
    @if ($__slidertype == 'slider')
        <div class="container-fluid container-menu-padding" style="position: absolute; z-index: 9999;">
    @else
        <div class="container container-menu-padding">
    @endif
        
        <style type="text/css">
            .langTool{
                /*border: 1px solid #000;*/
                position: absolute;
                top: 0;
                width: 100%;
                z-index: 9999;

                font-size: 12px;
                text-align: right;
                line-height: 20px;
            }

            .langLink{
                color: #000;
                font-size: 12px;
            }

            .langLink:hover {
                color: #b81521;
                text-decoration: none;
            }
            /*
                @if($__headertheme == 'home')
                    .langLink{
                        color: #FFF;
                        font-size: 12px;
                    }

                    .langLink:hover {
                        color: #b81521;
                        text-decoration: none;
                    }
                @else
                    .langLink{
                        color: #000;
                        font-size: 12px;
                    }

                    .langLink:hover {
                        color: #b81521;
                        text-decoration: none;
                    }
                @endif
            */
        </style>
        
        <div class="container container-menu-height container-menu-padding">
            
            <nav class="navbar navbar-expand-lg @if($__headertheme == 'home') navbar-light @else navbar-light p-top-20 @endif ">

                @if($__headertheme != 'home')
                    <div class="langTool">
                        @foreach ($langs as $lng)
                            <a class="langLink" href="{{ url($lng->code.$segments) }}">
                                {{ $lng->name }}
                            </a>
                            @if (!$loop->last)
                                <span class="langLink">|</span>
                            @endif
                        @endforeach
                    </div>
                @endif

                <a class="navbar-brand " href="{{ url('/') }}">
                    <img src="@if($__headertheme == 'home') {{ url('images/logo-black-'.Request::segment(1).'.svg') }} @else {{ url('images/logo-black-'.Request::segment(1).'.svg') }} @endif" height="100" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <style type="text/css">

                    .dropdown-submenu {
                      position: relative;
                    }

                    .dropdown-submenu a::after {
                      transform: rotate(-90deg);
                      position: absolute;
                      right: 6px;
                      top: .8em;
                    }

                    .dropdown-submenu .dropdown-menu {
                      top: 0;
                      left: 100%;
                      margin-left: .1rem;
                      margin-right: .1rem;
                    }

                    .dropright-menu {
                        top: 100% !important;
                        left: 0 !important;
                    }

                    .dropdown-item{
                        padding: 0.25rem 0.5rem;
                    }

                    @media (min-width: 991px){
                        .only-mobile {
                            display: none;
                        }
                    }
                </style>
                
                <div class="navbar-collapse collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto ust-menu @if($__headertheme == 'home')  @endif">
                        @php $i = 0; @endphp
                        @foreach ($topmenus as $menuitem)
                            @php
                                if (empty($menuitem->variableLang($lang))) {
                                    $menuitemVariable = $menuitem->variable;
                                }else{
                                    $menuitemVariable = $menuitem->variableLang($lang);
                                }
                            @endphp

                            @include('partials.headermenu')
                            @if (!$loop->last)
                                @if ($menuitem->position != 'none')
                                    <li role="separator" class="divider">&nbsp;|&nbsp;</li>
                                @endif
                            @endif

                        @endforeach

                        @if (Request::segment(1) == 'en')
                            <li class="nav-item only-mobile"><a class="nav-link" href="https://tahsilat.termorad.com.tr" target="_blank">ONLINE PAYMENT</a></li>
                            <li class="nav-item only-mobile"><a class="nav-link" href="https://www.dreamreality.com.tr/3d-model/termorad-unicera-2019/fullscreen" target="_blank">VIRTUAL TOUR</a></li>
                        @elseif (Request::segment(1) == 'de')
                            <li class="nav-item only-mobile"><a class="nav-link" href="https://tahsilat.termorad.com.tr" target="_blank">ONLINE ZAHLUNG</a></li>
                            <li class="nav-item only-mobile"><a class="nav-link" href="https://www.dreamreality.com.tr/3d-model/termorad-unicera-2019/fullscreen" target="_blank">3D PRODUKTE</a></li>
                        @else
                            <li class="nav-item only-mobile"><a class="nav-link" href="https://tahsilat.termorad.com.tr" target="_blank">ONLINE ÖDEME</a></li>
                            <li class="nav-item only-mobile"><a class="nav-link" href="https://www.dreamreality.com.tr/3d-model/termorad-unicera-2019/fullscreen" target="_blank">SANAL TUR</a></li>
                        @endif

                        
                      
                    </ul>
                </div>

            </nav>
            
        </div>
    </div>

    @include('partials.slidertype')
</header>