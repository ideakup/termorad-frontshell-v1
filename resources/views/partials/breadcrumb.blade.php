@if ($__breadcrumbvisible == 'yes')
    <div class="container-fluid breadcrumb-bg-color">
        <div class="container">
            <div class="row">
                @if ($__asidevisible == 'yes')
                    <div class="col-2 d-md-none d-lg-block d-sm-none d-none bread-padding"> &nbsp;</div>
                @endif
                <div class="@if ($__asidevisible == 'yes') col-9 col-sm p-left-y5 @else col-12 col-sm @endif">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url($lang.'/'.$mainmenu->variableLang($lang)->slug) }}">{{ $mainmenu->variableLang($lang)->title }}</a></li>

                        @if ($mainmenu->variableLang($lang)->slug != $menu->variableLang($lang)->slug)
                            <li class="breadcrumb-item active" aria-current="page">
                                <a href="{{ url($lang.'/'.$menu->variableLang($lang)->slug) }}">{{ $menu->variableLang($lang)->title }}</a>
                            </li>
                        @endif

                    </ol>
                </div>
            </div>
        </div>
    </div>
@endif