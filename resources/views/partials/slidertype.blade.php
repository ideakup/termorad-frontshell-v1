@switch($__slidertype)
    @case("image")
        <div class="row no-margin-padding banner-bg">
            <div class="col-lg-12 col-md-12 no-margin-padding banner-style" style="background-image: url({{ env('APP_UPLOAD_PATH_V3') }}/xlarge/{{ $menu->variableLang($lang)->stvalue }});">

                <div class="container-fluid" style="position: absolute; bottom: 0;">
                    <div class="container">
                        <div class="row">
                            @if ($__asidevisible == 'yes')
                                <div class="col-2 d-md-none d-lg-block d-sm-none d-none bread-padding"> &nbsp;</div>
                            @endif
                            <div class="@if ($__asidevisible == 'yes') col-9 col-sm p-left-y5 @else col-12 col-sm @endif">
                                

                                    @if ($mainmenu->variableLang($lang)->slug != $menu->variableLang($lang)->slug)
                                        
                                            <h1 style="color: #FFFFFF">{{ $menu->variableLang($lang)->title }}</h1>
                                        
                                    @endif

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        @break

    @case("slider")
            <div class="bd-news">
            
                <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel" data-interval="3000">
                        <ol class="carousel-indicators news-ind">

                            @foreach (App\Menu::find(44)->content as $haber)
                                <li data-target="#carouselExampleCaptions" data-slide-to="{{$loop->index}}" @if ($loop->first) class="active" @endif></li>
                            @endforeach

                        </ol>
                    <div class="carousel-inner">
                        
                        @foreach (App\Menu::find(44)->content as $haber)

                            <div class="carousel-item  @if ($loop->first) active @endif">
                                <img src="{{ url('images/spacer.png') }}" class="d-block w-100" alt="..." width="350" height="150">
                                <div class="carousel-caption d-md-block news-cap matchh">
                                    <p style="font-weight: 600;">{{$haber->variableLang($lang)->title}}</p>
                                    <p>{{$haber->variableLang($lang)->short_content}}</p>
                                </div>
                            </div>
                        @endforeach
                        
                    </div>
                </div>
            
            </div>

            <div class="bd-slider">

                <style type="text/css">
                    @media (max-width: 575px) {
                        .slider-height{
                            height: 350px;
                        }

                        .sli-cap {
                            margin-left: 5%;
                            width: 30%;
                            top: 138px;
                            bottom: unset;
                        }

                        .sli-cap h5 {
                            font-weight: 800;
                            font-size: 20px;
                            line-height: 22px;
                        }

                        .bd-news {
                            width: 30%;
                            margin-left: 50%;
                            margin-top: 138px;
                        }
                    }

                    @media (min-width: 576px) {
                        .slider-height{
                            height: 450px;
                        }

                        .sli-cap {
                            margin-left: 5%;
                            width: 25%;
                            top: 238px;
                            bottom: unset;
                        }

                        .sli-cap h5 {
                            font-weight: 800;
                            font-size: 25px;
                            line-height: 27px;
                        }

                        .bd-news {
                            margin-left: 45%;
                            margin-top: 238px;
                        }
                    }

                    @media (min-width: 768px) {
                        .slider-height{
                            height: 550px;
                        }

                        .sli-cap {
                            margin-left: 5%;
                            width: 25%;
                            top: 338px;
                            bottom: unset;
                        }

                        .sli-cap h5 {
                            font-weight: 800;
                            font-size: 35px;
                            line-height: 37px;
                        }

                        .bd-news {
                            margin-left: 45%;
                            margin-top: 338px;
                        }
                    }

                    @media (min-width: 992px) {
                        .slider-height{
                            height: 650px;
                        }

                        .sli-cap {
                            margin-left: 5%;
                            width: 25%;
                            top: 438px;
                            bottom: unset;
                        }

                        .bd-news {
                            margin-left: 45%;
                            margin-top: 438px;
                        }
                    }

                    @media (min-width: 1200px) {
                        .slider-height{
                            height: 750px;
                        }

                        .sli-cap {
                            margin-left: 5%;
                            width: 25%;
                            top: 538px;
                            bottom: unset;
                        }

                        .bd-news {
                            margin-left: 45%;
                            margin-top: 538px;
                        }
                    }
                </style>
            
                <div id="carouselExampleCaptions2" class="carousel slide" data-ride="carousel" data-interval="5000">
                    <!--
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                            <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                        </ol>
                    -->
                    <div class="carousel-inner">
                        
                        @foreach ($menu->slider as $sElement)
                            
                            @php
                                if (empty($sElement->variableLang($lang))) {
                                    $elementVariable = $sElement->variable;
                                }else{
                                    $elementVariable = $sElement->variableLang($lang);
                                }
                            @endphp

                            <div class="carousel-item @if ($loop->first) active @endif">
                                @if (!is_null($elementVariable->image_url))
                                    <img src="{{ env('APP_UPLOAD_PATH_V3') }}/xlarge/{{ $elementVariable->image_url }}" class="d-block w-100 slider-height" alt="...">
                                @endif
                                
                                <div class="carousel-caption d-md-block sli-cap matchh">
                                    <h5>{!! $elementVariable->title !!}</h5>
                                    <p>{!! $elementVariable->description !!}</p>
                                </div>

                            </div>
                            @if (!empty($elementVariable->button_text))
                                {{-- $elementVariable->button_url --}}
                                {{-- $elementVariable->button_text --}}
                            @endif
                        @endforeach

                        <!--
                            <div class="carousel-item active">
                                <img src="http://termorad_frontshell.local/images/slidebg.jpg" class="d-block w-100" alt="..." height="750">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>First slide label</h5>
                                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="http://termorad_frontshell.local/images/slidebg.jpg" class="d-block w-100" alt="..." height="750">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Second slide label</h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="http://termorad_frontshell.local/images/slidebg.jpg" class="d-block w-100" alt="..." height="750">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Third slide label</h5>
                                    <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                                </div>
                            </div>
                        -->
                    </div>

                    <a class="carousel-control-prev" href="#carouselExampleCaptions2" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleCaptions2" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            
            </div>


        @break

    @default
        
@endswitch