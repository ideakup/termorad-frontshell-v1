@extends('layouts.app')

@section('content')
    
    <?php
        $_masterColContentClasses = '';
        $_rowContentClasses = '';
        $_colContentClasses = '';
        $_rowBGImageUrlStyle = '';

        if ($menu->asidevisible == 'yes'){
            $_masterColContentClasses = 'col-10 col-sm p-left-y5'; 
        } else {
            if ($mainmenu->variableLang(Request::segment(1))->slug == $menu->variableLang(Request::segment(1))->slug) {
                $_masterColContentClasses = 'col-md-10 offset-md-1 col-sm-12'; 
            } else {
                $_masterColContentClasses = 'col-12 col-sm'; 
            }
        }
    ?>

    <div class='{{ $_masterColContentClasses }} master-col-content'>
        <!--
            @if ($mainmenu->variableLang(Request::segment(1))->slug != $menu->variableLang(Request::segment(1))->slug)
                <div class='row'>
                    <div class='col-12'>
                        <h2> {{ $menu->variableLang(Request::segment(1))->name }} </h2>
                    </div>
                </div>
            @endif
        -->
        
        @foreach ($menu->content as $cont)

            <?php
                if (empty($cont->variableLang(Request::segment(1)))) {
                    $contVariable = $cont->variable;
                }else{
                    $contVariable = $cont->variableLang(Request::segment(1));
                }

                if ($contVariable->row == 'normal'){
                    $_rowContentClasses = 'row ';
                    $_colContentClasses = '';
                } else if ($contVariable->row == 'full'){ 
                    $_rowContentClasses = 'row-full ';
                    $_colContentClasses = 'p-lr-0 '; 
                }

                if (is_null($contVariable->bgimageurl)){
                    $_colContentClasses .= '';
                    $_rowBGImageUrlStyle = '';
                } else { 
                    $_colContentClasses .= 'bg-image-container ';
                    $_rowBGImageUrlStyle .= 'background-image: url("'.env('APP_UPLOAD_PATH_V3').'xlarge/'.$contVariable->bgimageurl.'"); ';

                    if (!is_null($contVariable->height)){
                        $_colContentClasses .= '';
                        $_rowBGImageUrlStyle .= 'height: '.$contVariable->height.'px;';
                    }
                }
            ?>

            <div class="{{ $_rowContentClasses }}">
                <div class="col-12 margin-b-35 {{ $_colContentClasses }}" style="{{$_rowBGImageUrlStyle}}">
                    @if ($cont->type == 'text')
                            {!! $contVariable->content !!}
                    @elseif ($cont->type == 'photo')
                        @if ($contVariable->props == 'responsive')
                            <p style="text-align: center;"><img class="img-fluid" src="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ $contVariable->content }}"></p>
                        @else
                            <p style="text-align: center;"><img src="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ $contVariable->content }}"></p>
                        @endif 
                    @elseif ($cont->type == 'photogallery')
                        @foreach ($cont->photogallery as $img)
                            <div class="
                                @if ($contVariable->props == 'col-2x')
                                    col-6
                                @elseif ($contVariable->props == 'col-3x')
                                    col-4
                                @elseif ($contVariable->props == 'col-4x')
                                    col-3
                                @endif
                            " style="float: left; padding: 15px;">
                                <a href="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ $img->url }}" data-lightbox="lightbox-{{$cont->id}}" data-title="{{ $img->name }}">
                                    <img class="img-fluid" src="{{ env('APP_UPLOAD_PATH_V3') }}thumbnail2x/{{ $img->url }}" alt="{{ $img->name }}">
                                </a>
                            </div>
                        @endforeach

                    @elseif ($cont->type == 'link')
                        <p style="text-align: center;">
                            <a href="{{ $contVariable->content }}" {{ ($contVariable->props == 'external') ? 'target="_blank"' : '' }} class="moreButtonBlack">
                                @if (Request::segment(1) == 'tr')
                                    Daha Fazla Bilgi Al
                                @elseif(Request::segment(1) == 'en')
                                    Read More
                                @elseif(Request::segment(1) == 'de')
                                    Erfahren Sie Mehr
                                @endif
                            </a>
                        </p>
                    @endif

                </div>
            </div>

        @endforeach

    </div>

@endsection

@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function() {

            $('.matchh').matchHeight();

        });
    </script>
@endsection