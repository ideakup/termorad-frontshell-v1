<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <title>{{ $sitesettings->where('slug', 'seo-title')->first()->value }} - {{ $menu->variableLang(Request::segment(1))->title }}</title>

    <meta name="title" content="{{ $sitesettings->where('slug', 'seo-title')->first()->value }}">
    <meta name="description" content="{{ $sitesettings->where('slug', 'seo-description')->first()->value }}">
    <meta name="keywords" content="{{ $sitesettings->where('slug', 'seo-keyword')->first()->value }}">

    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="apple-touch-icon" sizes="57x57" href="{{ url('images/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ url('images/favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ url('images/favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('images/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ url('images/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ url('images/favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ url('images/favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ url('images/favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('images/favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ url('images/favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('images/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ url('images/favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('images/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ url('images/favicon/manifest.json') }}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed|Roboto:400,700,900&amp;subset=latin-ext" rel="stylesheet">
    <!--
    <style type="text/css">

        @font-face {
            font-family: 'AlrightSans-Regular';
            src: url('/fonts/AlrightSans-Regular.eot?#iefix') format('embedded-opentype'), url('/fonts/AlrightSans-Regular.otf') format('opentype'), url('/fonts/AlrightSans-Regular.woff') format('woff'), url('/fonts/AlrightSans-Regular.ttf') format('truetype'), url('/fonts/AlrightSans-Regular.svg#AlrightSans-Regular') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face {
            font-family: 'AlrightSans-Bold';
            src: url('/fonts/AlrightSans-Bold.eot?#iefix') format('embedded-opentype'), url('/fonts/AlrightSans-Bold.otf') format('opentype'), url('/fonts/AlrightSans-Bold.woff') format('woff'), url('/fonts/AlrightSans-Bold.ttf') format('truetype'), url('/fonts/AlrightSans-Bold.svg#AlrightSans-Bold') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face {
            font-family: 'AlrightSans-ExtraThin';
            src: url('/fonts/AlrightSans-ExtraThin.eot?#iefix') format('embedded-opentype'), url('/fonts/AlrightSans-ExtraThin.otf') format('opentype'), url('/fonts/AlrightSans-ExtraThin.woff') format('woff'), url('/fonts/AlrightSans-ExtraThin.ttf') format('truetype'), url('/fonts/AlrightSans-ExtraThin.svg#AlrightSans-ExtraThin') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face {
            font-family: 'AlrightSans-Thin';
            src: url('/fonts/AlrightSans-Thin.eot?#iefix') format('embedded-opentype'), url('/fonts/AlrightSans-Thin.otf') format('opentype'), url('/fonts/AlrightSans-Thin.woff') format('woff'), url('/fonts/AlrightSans-Thin.ttf') format('truetype'), url('/fonts/AlrightSans-Thin.svg#AlrightSans-Thin') format('svg');
            font-weight: normal;
            font-style: normal;
        }
        
    </style>
    -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <style type="text/css">
        .tabTable a.nav-link{
            font-family: 'Open Sans', sans-serif;
            font-weight: 300;
            color: white;
            font-size: 14px;
            padding: 0 4px;
        }

        .tabTable .divider{
            padding: 0;
        }

        #pills-tab{
            margin-top: 1rem;
            display: block;
        }

        .nav-tabs > li, .nav-pills > li {
            float:none;
            display:inline-block;
            zoom:1;
        }

        .nav-tabs, .nav-pills {
            text-align:center;
            display:inline-block;
        }
        .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
            color: #e74639;
            background-color: unset;
        }

    </style>
</head>
<body>
    @php
        $__headertheme = $menu->headertheme;
        $__slidertype = $menu->slidertype;
        $__breadcrumbvisible = $menu->breadcrumbvisible;
        $__asidevisible = $menu->asidevisible;
    @endphp
    @include('partials.header')
    @include('partials.breadcrumb')
    
    <main class="container">
        
        <div class="row margin-tb-35 master-row-content">
            
            @include('partials.asidebar')

            @yield('content')

        </div>
        
    </main>

    @include('partials.footer')
    
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    
    @yield('inline-scripts')

    <script type="text/javascript">
        {{ $sitesettings->where('slug', 'google-analytics-code')->first()->value }}
    </script>

    <script type="text/javascript">
        $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
            if (!$(this).next().hasClass('show')) {
                $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
            }
            var $subMenu = $(this).next(".dropdown-menu");
            $subMenu.toggleClass('show');

            $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
                $('.dropdown-submenu .show').removeClass("show");
            });

            return false;
        });

        $('.bd-slider').carousel({
          interval: 3000
        });

        $(".bd-slider").on('slide.bs.carousel', function(){
            console.log('A new slide is about to be shown!');
        });
        
    </script>
</body>
</html>
